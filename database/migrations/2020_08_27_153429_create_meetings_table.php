<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meetings', function (Blueprint $table) {
            $table->id();
            $table->string('type',30)->nullable();
            $table->string('place',150);
            $table->date('date');
            $table->time('start');
            $table->time('end');
            $table->string('description',500)->nullable();
            $table->string('summary',500)->nullable();
            $table->string('equipment',500)->nullable();
            $table->bigInteger('receipt_id')->nullable()->unsigned()->index;
            $table->bigInteger('service_id')->nullable()->unsigned()->index;
            $table->bigInteger('customer_id')->nullable()->unsigned()->index;
            $table->bigInteger('supplier_id')->nullable()->unsigned()->index;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meetings');
    }
}

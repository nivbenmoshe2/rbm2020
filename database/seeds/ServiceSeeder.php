<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            [
                'name' => 'Interior design',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Coaching',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],  
            [
                'name' => 'Private lessons',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [
                'name' => 'Team guideness',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                 
            ]);
    }
}

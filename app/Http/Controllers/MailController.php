<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Supplier;
use App\Meeting;
use App\Service;
use App\Mail\meetingMail;
use Mail;
use Session;

class MailController extends Controller
{

    public function mail($id)
    {
        $meeting = Meeting::findOrFail($id);
        $suppliers = Supplier::all(); 
        $customers = Customer::all();      
        $services = Service::all();     
        return view('meetings.mail', compact('meeting','suppliers','customers','services'));
    }

    public function sendemail(Request $get)
    {
        $url = substr(str_replace(url('/mail'), '', url()->previous()),1);
        $this->validate($get,[
            "email"=>"required",
            "message"=>"required",
        ]);
            
        $email=$get->email;
        $message=$get->message;

        Mail::to($email)->send(new meetingMail($message));
        Session::flash("Successfull");
        Meeting::where('id',$url)->update(['statusmail'=>1]);
        return back();
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

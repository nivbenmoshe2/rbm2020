<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Collection;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('admin');
        $roles = Role::all();
        $users = User::where('role_id',2)->get();
        $customers = Customer::all();        
        return view('users.index', compact('users','customers','roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('admin');
        $roles = Role::all();
        $customers = Customer::all();
        return view('users.create', compact('customers','roles'));
    }

    public function changeCustomer($uid, $cid = null){
        Gate::authorize('admin');
        $user = User::findOrFail($uid);
        $user['customer_id'] = $cid;
        $user->save(); 
        return back();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $us = $user->create($request->all());
        $us->password = Hash::make($request['password']);
        $us->save();
        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('admin');
        $roles = Role::all();
        $user = User::findOrFail($id);
        $customers = Customer::all();
        return view('users.edit', compact('user','customers','roles'));
    }

    public function editAdmin()
    {
        Gate::authorize('admin');
        $id = Auth::id();
        $roles = Role::all();
        $user = User::findOrFail($id);
        $customers = Customer::all();
        return view('users.edit', compact('user','customers','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Gate::authorize('admin');
        $user = user::findOrFail($id);
       if(!isset($request->password)){
        $request['password'] = $user->password;   
       }else{
         $request['password']= Hash::make($request['password']);   
       } 
       $user->update($request->all());
       return redirect('users'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('admin');
        $user = User::findOrFail($id);
        $user->delete(); 
        return redirect('users'); 
    }
}

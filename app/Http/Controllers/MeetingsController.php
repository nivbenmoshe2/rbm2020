<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Receipt;
use App\User;
use App\Customer;
use App\Supplier;
use App\Meeting;
use App\Service;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Mail;
use Carbon\Carbon;

class MeetingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('admin');
        $meetings = Meeting::orderBy('date', 'ASC')->paginate(5);
        $suppliers = Supplier::all();
        $receipts = Receipt::all();  
        $customers = Customer::all();      
        $services = Service::all();     
        return view('meetings.index', compact('meetings','suppliers','receipts','customers','services'));
    }

    public function search(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        if(!isset($from)){
            $from = '2000-01-01';
        }
        if(!isset($to)){
            $to = '2100-01-01';
        }
        $meetings = Meeting::orderBy('date', 'ASC')->orderBy('start', 'ASC')->whereBetween('date',[$from,$to])->paginate(5);   
        return view('meetings.index', compact('meetings'));
    }

    public function searchCustomer(Request $request)
    {
        $name = $request->search;
        $id = Customer::where('name','LIKE','%'.$name.'%')->pluck('id');
        if($id->isEmpty()){
            Session::flash("Sorry");
            return redirect()->back();
        }
        $meetings = Meeting::orderBy('date', 'ASC')->orderBy('start', 'ASC')->whereIn('customer_id',$id)->paginate(10);  
        $customers = Customer::all();  
        return view('meetings.business', compact('meetings','customers'));
    }

    public function personal()
    {
        Gate::authorize('admin');
        $meetings = Meeting::orderBy('created_at', 'DESC')->where('type','Personal')->paginate(5);
        $suppliers = Supplier::all();
        $receipts = Receipt::all();  
        $customers = Customer::all();      
        $services = Service::all();     
        return view('meetings.personal', compact('meetings','suppliers','receipts','customers','services'));
    }

    public function business()
    {
        Gate::authorize('admin');
        $meetings = Meeting::orderBy('created_at', 'DESC')->where('type','Business')->paginate(5);
        $suppliers = Supplier::all();
        $receipts = Receipt::all();  
        $customers = Customer::all();      
        $services = Service::all();     
        return view('meetings.business', compact('meetings','suppliers','receipts','customers','services'));
    }

    
    
    public function myMeetings()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $customer = $user->customer;
        $meetings = $customer->meetings;
        $suppliers = Supplier::all();
        $receipts = Receipt::all();  
        $customers = Customer::all();      
        $services = Service::all();     
        return view('meetings.my', compact('meetings','suppliers','receipts','customers','services')); 
    }

    public function myDebts()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $customer = $user->customer;
        $meetings = $customer->meetings;
        $suppliers = Supplier::all();
        $receipts = Receipt::all();  
        $customers = Customer::all();      
        $services = Service::all();     
        return view('meetings.mydebts', compact('meetings','suppliers','receipts','customers','services')); 
    }

    public function debts()
    {        
        Gate::authorize('admin');
        $meetings = Meeting::wherenull('receipt_id')->wherenotnull('summary')->orderBy('date', 'DESC')->orderBy('start', 'ASC')->where('type','Business')->paginate(5);
        $suppliers = Supplier::all();
        $receipts = Receipt::all();  
        $customers = Customer::all();      
        $services = Service::all();     
        return view('meetings.debts', compact('meetings','suppliers','receipts','customers','services')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('admin');
        $suppliers = Supplier::where('status_id',1)->get();
        $receipts = Receipt::all();  
        $customers = Customer::where('status_id',1)->get();      
        $services = Service::all();     
        
        return view('meetings.create', compact('suppliers','receipts','customers','services'));
    }

    public function add()
    {   
        Gate::authorize('admin');
        return view('meetings.add');
    }

    public function schedule()
    {
        $user = Auth::user()->role_id;
        $customer = Auth::user()->customer_id;
        $now = Carbon::now();
        $weekStartDate = $now->startOfWeek(Carbon::SATURDAY)->format('Y-m-d');
        $weekEndDate = $now->endOfWeek(Carbon::FRIDAY)->format('Y-m-d');
        if(Auth::user()->role_id == 1){
            $meetings = Meeting::orderBy('date', 'ASC')->orderBy('start', 'ASC')->whereBetween('date',[$weekStartDate,$weekEndDate])->paginate(10);   
        }
        else{
            $meetings = Meeting::where('customer_id',$customer)->orderBy('date', 'ASC')->orderBy('start', 'ASC')->whereBetween('date',[$weekStartDate,$weekEndDate])->paginate(10);
        }
        return view('meetings.schedule', compact('meetings','weekStartDate','weekEndDate')); 
        
    }

    

    public function close($id)
    {   
        Gate::authorize('admin');
        $meeting = Meeting::findOrFail($id);
        return view('meetings.close', compact('meeting'));
    }

    public function closePersonal($id)
    {   
        Gate::authorize('admin');
        $meeting = Meeting::findOrFail($id);
        $meeting->summary = 1;
        $meeting->save();
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $meeting = new Meeting();
        $met = $meeting->create($request->all());
        $url = str_replace(url('/'), '', url()->previous());
        if($url == '/add'){
            $met->type = 'Personal';
            $met->save();
            return redirect('personal');
        } 
        else{  
            $met->type = 'Business';
            $met->save();
            return redirect('business');
        }
    }

    public function multiplerecords(Request $req)
    {
        $url = substr(str_replace(url('/payment'), '', url()->previous()),1);
        $key = $req->id;
        if($key == null){
            return redirect()->back();
        }
        foreach ($key as $ke) {
            Meeting::where('id', $ke)->update(['receipt_id'=>$url]);
       }
        Receipt::where('id',$url)->update(['status'=>1]);
        Session::flash("Successfull");
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $meeting = Meeting::findOrFail($id);
        return view('meetings.show', compact('meeting'));
    }

    public function multipledelete(Request $req)
    {
        $key = $req->id;
        if($key == null){
            return redirect()->back();
        }
        foreach ($key as $ke) {
            Meeting::where('id', $ke)->delete();
        }
        Session::flash("Delete");
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('admin');
        $meeting = Meeting::findOrFail($id);
        $suppliers = Supplier::where('status_id',1)->get();
        $receipts = Receipt::all();  
        $customers = Customer::where('status_id',1)->get();      
        $services = Service::all();     
        return view('meetings.edit', compact('meeting','suppliers','receipts','customers','services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $meeting = Meeting::findOrFail($id);
        $meeting->update($request->all());
        Session::flash("Successfull");
        return redirect()->back();
    }

   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('admin');
        $meeting = Meeting::findOrFail($id);
        $meeting->delete();
        Session::flash("Delete");
        return redirect()->back();
    }
}

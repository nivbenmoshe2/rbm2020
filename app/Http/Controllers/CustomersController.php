<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\User;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('admin');
        $customers = Customer::paginate(5);
        $statuses = Status::all();        
        return view('customers.index', compact('customers', 'statuses'));
    }

    public function active()
    {
        Gate::authorize('admin');
        $customers = Customer::where('status_id',1)->paginate(5);
        $statuses = Status::all();        
        return view('customers.index', compact('customers', 'statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function changeStatus($cid, $sid)
    {
        Gate::authorize('admin');
        $customer = Customer::findOrFail($cid);        
        $customer->status_id = $sid;
        $customer->save();
        Session::flash("Successfull");
        return back();
    }  



    public function create()
    {
        Gate::authorize('admin');
        $statuses = Status::all();
        return view('customers.create',compact('statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customer();
        $cus = $customer->create($request->all());
        $cus->status_id = 1;
        $cus->save();
        return redirect('customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::findOrFail($id);
        return view('customers.show', compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('admin');
        $customer = Customer::findOrFail($id);
        return view('customers.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);
        $customer->update($request->all());
        Session::flash("Successfull");
        return redirect('customers');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('admin');
        $customer = Customer::findOrFail($id);
        $user = User::where('customer_id','=',$id)->delete(); 
        $customer->delete(); 
        return redirect('customers');
    }
}

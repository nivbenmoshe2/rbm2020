<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\Category;
use App\Supplier;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

class SuppliersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('admin');
        $suppliers = Supplier::paginate(5);
        $statuses = Status::all();  
        $categories = Category::all();      
        return view('suppliers.index', compact('suppliers', 'statuses','categories'));
    }

    public function active()
    {
        Gate::authorize('admin');
        $suppliers = Supplier::where('status_id',1)->paginate(5);
        $statuses = Status::all();   
        $categories = Category::all();     
        return view('suppliers.index', compact('suppliers', 'statuses','categories'));
    }


    public function changeStatus($id, $sid)
    {
        Gate::authorize('admin');
        $supplier = Supplier::findOrFail($id);        
        $supplier->status_id = $sid;
        $supplier->save();
        Session::flash("Successfull");
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('admin');
        $statuses = Status::all();
        $categories = Category::all();
        return view('suppliers.create',compact('statuses','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $supplier = new Supplier();
        $sup = $supplier->create($request->all());
        $sup->status_id = 1;
        $sup->save();
        return redirect('suppliers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supplier = Supplier::findOrFail($id);
        return view('suppliers.show', compact('supplier'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('admin');
        $supplier = Supplier::findOrFail($id);
        $categories = Category::all();
        return view('suppliers.edit', compact('supplier','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Supplier::findOrFail($id);
        $supplier->update($request->all());
        Session::flash("Successfull");
        return redirect('suppliers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('admin');
        $supplier = Supplier::findOrFail($id);
        $supplier->delete(); 
        return redirect('suppliers');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function meetings()
    {
        return $this->hasMany('App\Meeting');
    }
}

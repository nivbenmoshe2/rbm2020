<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = ['name','phone','contact','address','commission','category_id'];

    public function status(){
        return $this->belongsTo('App\Status');
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function meetings()
    {
        return $this->hasMany('App\Meeting');
    }


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    protected $fillable = ['number','cash','bit','total','date','customer_id'];


    public function meetings()
    {
        return $this->hasMany('App\Meeting');
    }

    public function customer(){
        return $this->belongsTo('App\Customer');
    }
}

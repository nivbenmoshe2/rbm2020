<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role_id','customer_id',
    ];

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function customer(){
        return $this->belongsTo('App\Customer');
    }
    
    public function isAdmin(){
        $role = $this->role;
        if($role->name === 'admin') return true; 
        return false; 
    }

    public function isCustomer(){
        $roles = $this->roles;
        if($role->name === 'customer') return true;
        return false; 
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}

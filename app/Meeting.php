<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $fillable = ['place','type','date','start','end','description','equipment','service_id','customer_id','supplier_id','receipt_id','summary'];


    public function service(){
        return $this->belongsTo('App\Service');
    }

    public function supplier(){
        return $this->belongsTo('App\Supplier');
    }

    public function receipt(){
        return $this->belongsTo('App\Receipt');
    }

    public function customer(){
        return $this->belongsTo('App\Customer');
    }
    

}

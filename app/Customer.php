<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['name','phone','birthdate','address','gender','email'];

    public function user(){
        return $this->hasMany('App\User');
    }

    public function status(){
        return $this->belongsTo('App\Status');
    }

    public function meetings()
    {
        return $this->hasMany('App\Meeting');
    }

    public function receipts(){
        return $this->hasMany('App\Receipt');
    }
}

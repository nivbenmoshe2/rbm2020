<nav class="navbar navbar-vertical fixed-left navbar-expand-md" style="background-color: #79ebe0b4" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        
        <span  class="navbar-brand pt-0" href="#">
            <span class="avatar avatar-sm rounded-circle">
            <img alt="Image placeholder" src="/rbm2020/public/img/rbm.png">
            </span>
            <p>{{ auth()->user()->name }}</p>
            
        </span >
       
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
          
            <!-- Form -->
            
            <!-- Navigation -->
            <ul class="navbar-nav">
                <div class="dropdown-divider"></div>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/schedule') }}"><i class="far fa-calendar-alt"></i>
                        Weekly Schedule
                    </a>
                </li>
                @cannot('admin')
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/mymeetings') }}"><i class="far fa-handshake"></i>
                        My meetings
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/myreceipts') }}"><i class="fas fa-receipt"></i>
                        My receipts
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/mydebts') }}"><i class="fas fa-money-check-alt"></i>
                        My debts
                    </a>
                </li>
                @endcannot
                
                @can('admin')
                <li class="nav-item">
                    <a class="nav-link " href="#meetings" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="meetings">
                        <i class="fas fa-handshake"></i>
                        <span class="nav-link-text" >Meetings</span>
                    </a>
                    <div class="collapse" id="meetings">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('meetings.personal') }}"><i class="fas fa-people-arrows"></i>
                                    Personal
                                 </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('meetings.business') }}"><i class="fas fa-business-time"></i>
                                    Business
                                 </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('meetings.debts') }}"><i class="fas fa-money-check-alt"></i>
                                    Debts
                                 </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/meetings') }}"><i class="fas fa-handshake"></i>
                                   All Meetings
                                </a>
                            </li>
                        </ul>
                    </div>  
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#receipts" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="receipts">
                        <i class="fas fa-receipt"></i>
                        <span class="nav-link-text" >Receipts</span>
                    </a>
                    <div class="collapse" id="receipts">
                        <ul class="nav nav-sm flex-column">
        
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('receipts/create') }}"><i class="fas fa-folder-plus"></i>
                                    Add Receipt
                                 </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/receipts') }}"><i class="fas fa-receipt"></i>
                                    All Receipts
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#customers" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="customers">
                        <i class="fab fa-intercom"></i>
                        <span class="nav-link-text" >Customers</span>
                    </a>
                    <div class="collapse" id="customers">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('customers/create') }}"><i class="fas fa-business-time"></i>
                                    Add Customer
                                 </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('activecustomers') }}"><i class="fas fa-people-arrows"></i>
                                    Active Customers
                                 </a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/customers') }}"><i class="fab fa-intercom"></i>
                                    All Customers
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link " href="#suppliers" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="suppliers">
                        <i class="fas fa-parachute-box"></i>
                        <span class="nav-link-text" >Suppliers</span>
                    </a>
                    <div class="collapse" id="suppliers">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('suppliers/create') }}"><i class="fas fa-business-time"></i>
                                    Add supplier
                                 </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('activesuppliers') }}"><i class="fas fa-people-arrows"></i>
                                    Active Suppliers
                                 </a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/suppliers') }}"><i class="fas fa-parachute-box"></i>
                                    All suppliers
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#users" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="users">
                        <i class="fas fa-users"></i>
                        <span class="nav-link-text" >Users</span>
                    </a>
                    <div class="collapse" id="users">
                        <ul class="nav nav-sm flex-column">
        
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/users/create')}}"><i class="fas fa-user-plus"></i>
                                    Add user
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/users') }}"><i class="fas fa-users"></i>
                                    All Users
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                @endcan
                <div class="dropdown-divider"></div>
                @can('admin')
                <li class="nav-item">
                    <a class="dropdown-item" href="{{ url('/editadmin') }}"><i class="fas fa-user-edit"></i>
                        Edit Profile
                    </a>
                </li>
                @endcan
                <li class="nav-item">
                    
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i>
                            <span>{{ __('Logout') }}</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                <img src="/rbm2020/public/img/logo-rbm.jpeg" height="120" width="200"> 
            </ul>
        </div>
    </div>
</nav>
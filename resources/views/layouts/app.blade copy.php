<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://kit.fontawesome.com/430b0c0e38.js" crossorigin="anonymous"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/mymeetings') }}"><i class="far fa-handshake"></i>
                    My meetings
                </a>
                <a class="navbar-brand" href="{{ url('/schedule') }}"><i class="ni ni-tv-2 text-primary"></i>
                    Schedule
                </a>
                <a class="navbar-brand" href="{{ url('/myreceipts') }}"><i class="fas fa-receipt"></i>
                    My receipts
                </a>
                <a class="navbar-brand" href="{{ url('/mydebts') }}">
                    My debts
                </a>
                <a class="navbar-brand" href="{{ url('/customers') }}"><i class="fab fa-intercom"></i>
                    Customers
                </a>
                <a class="navbar-brand" href="{{ route('meetings.personal') }}"><i class="fas fa-people-arrows"></i>
                    Personal
                 </a>
                 <a class="navbar-brand" href="{{ route('meetings.business') }}"><i class="fas fa-business-time"></i>
                    Business
                 </a>
                <a class="navbar-brand" href="{{ url('/meetings') }}"><i class="fas fa-handshake"></i>
                    Meetings
                </a>
                <a class="navbar-brand" href="{{ url('/receipts') }}"><i class="fas fa-receipt"></i>
                    Receipts
                </a>
                @can('admin')
                <a class="navbar-brand" href="{{ url('/suppliers') }}"><i class="fas fa-parachute-box"></i>
                    Suppliers
                </a>
                @endcan
                @can('admin')
                <a class="navbar-brand" href="{{ url('/users') }}"><i class="fas fa-users"></i>
                    Users
                </a>
                @endcan
                @can('admin')
                <a class="navbar-brand" href="{{url('/users/create')}}"><i class="fas fa-user-plus"></i>
                        Add user
                </a>
                @endcan
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-5">
            @yield('content')
        </main>
    </div>
</body>
</html>

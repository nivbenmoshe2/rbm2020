@extends('layouts.app')

@section('title', 'Users')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-10">
                            <h2>List of users</h2>
                        </div>
                    </div>
                </div>        
                    <div class="card-body">
                        <table class = "table table-striped">
                            <tr>
                                <th>Name</th><th>Customer</th><th>Email</th><th>Role</th><th>Created</th><th>Updated</th><th></th>
                            </tr>
                            <!-- the table data -->
                            @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->name}}</td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    @if(isset($user->customer_id))
                                                    {{$user->customer->name}}  
                                                    @else
                                                    Assign customer
                                                    @endif
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                @foreach($customers as $customer)
                                                <a class="dropdown-item" href="{{route('user.changecustomer',[$user->id,$customer->id])}}">{{$customer->name}}</a>
                                                @endforeach
                                                </div>
                                            </div>                
                                        </td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->role->name}}</td>               
                                        <td>{{$user->created_at}}</td>
                                        <td>{{$user->updated_at}}</td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-dark" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <a class="dropdown-item" href="{{route('users.edit',$user->id)}}">Edit</a>
                                                    <a class="dropdown-item" href="{{route('user.delete',$user->id)}}">Delete</a>  
                                                </div>
                                            </div>
                                        </td>                                                               
                                    </tr>
                                    
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection


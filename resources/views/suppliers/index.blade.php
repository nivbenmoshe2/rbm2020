@extends('layouts.app')

@section('title', 'Suppliers')

@section('content')



<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-10">
                            <h2>Suppliers</h2>
                        </div>        
                        <a href =  "{{url('/suppliers/create')}}" class="btn btn-sm btn-info"> Add new supplier</a>
                    </div>
                </div>
                        <div class="card-body">
                            @if(Session::has("Successfull"))
                                <div class="alert alert-success">
                                    <b>Successfull, Your updates have been saved.</b>
                                </div>
                            @endif
                        <table class = "table table-striped">
                            <tr>
                                <th>Name</th><th>Phone</th><th>Contact</th><th>Commission</th><th>Status</th><th>Category</th><th></th>
                            </tr>
                            <!-- the table data -->
                            @foreach($suppliers as $supplier)
                                    <tr>
                                        <td>{{$supplier->name}}</td>
                                        <td>{{$supplier->phone}}</td>
                                        <td>{{$supplier->contact}}</td>
                                        <td>{{$supplier->commission}}%</td>
                                        <td><div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {{$supplier->status->name}}
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                @foreach($statuses as $status)
                                                <a class="dropdown-item" href="{{route('suppliers.changestatus', [$supplier->id,$status->id])}}">{{$status->name}}</a>
                                                @endforeach
                                                </div></td>
                                        <td>{{$supplier->category->name}}</td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-dark" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <a class="dropdown-item" href="{{route('suppliers.show',$supplier->id)}}">Details</a>
                                                    <a class="dropdown-item" href="{{route('suppliers.edit',$supplier->id)}}">Edit</a> 
                                                </div>
                                            </div>
                                        </td>                                                                          
                                    </tr>
                                    
                            @endforeach
                        </table>
                        {{$suppliers->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection


@extends('layouts.app')

@section('title', 'Create supplier')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create supplier</div>        
                    <div class="card-body">
                        <form method = "post" action = "{{action('SuppliersController@store')}}">
                        @csrf 
                        <div class="form-group">
                            <label for = "name">Name</label>
                            <input type = "text" class="form-control @error('name') is-invalid @enderror" name = "name" required autocomplete="name" autofocus>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> 
                        <div class="form-group">
                            <label for = "phone">Phone</label>
                            <input type = "number" class="form-control @error('phone') is-invalid @enderror" name = "phone" required autocomplete="phone" autofocus>
                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> 
                        <div class="form-group">
                            <label for = "contact">Contact</label>
                            <input type = "text" class="form-control @error('contact') is-invalid @enderror" name = "contact" required autocomplete="contact" autofocus>
                            @error('contact')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>    
                        <div class="form-group">
                            <label for = "address">Address</label>
                            <input type = "text" class="form-control" name = "address">
                        </div> 
                        <div class="form-group">
                            <label for = "commission">Commission</label>
                            <input type = "number" step = "0.01" class="form-control @error('commission') is-invalid @enderror" name = "commission" required autocomplete="commission" autofocus>
                            @error('commission')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group" >
                            <label for="category_id">Category</label>
                                <select class="form-control" name="category_id">                                                                         
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}"> 
                                        {{ $category->name }} 
                                    </option>
                                @endforeach    
                                </select>
                        </div>
                        <div>  
                            <input TYPE="button" class="btn btn-primary" VALUE="Back" onClick="history.go(-1);">
          
                            <input type = "submit" class="btn btn-primary" name = "submit" value = "Create supplier">
                        </div>                       
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection

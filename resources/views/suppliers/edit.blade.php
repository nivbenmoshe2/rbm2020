@extends('layouts.app')

@section('title', 'Edit supplier')

@section('content') 
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit supplier</div>        
                    <div class="card-body">      
                        <form method = "post" action = "{{action('SuppliersController@update',$supplier->id)}}">
                        @csrf
                        @METHOD('PATCH')
                        <div class="form-group">
                            <label for = "name">Supplier name</label>
                            <input type = "text" class="form-control @error('name') is-invalid @enderror" name = "name" value = {{$supplier->name}} required autocomplete="name" autofocus>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for = "phone">Supplier phone</label>
                            <input type = "number" class="form-control @error('phone') is-invalid @enderror" name = "phone" value = {{$supplier->phone}} required autocomplete="phone" autofocus>
                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>     
                        <div class="form-group">
                            <label for = "contact">Supplier contact</label>
                            <input type = "text" class="form-control @error('contact') is-invalid @enderror" name = "contact" value = {{$supplier->contact}} required autocomplete="contact" autofocus>
                            @error('contact')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> 
                        <div class="form-group">
                            <label for = "address">Supplier address</label>
                            <input type = "text" class="form-control" name = "address" value = {{$supplier->address}}>
                        </div>
                        <div class="form-group">
                            <label for = "commission">Supplier commission</label>
                            <input type = "number" step = "0.01" class="form-control @error('commission') is-invalid @enderror" name = "commission" value = {{$supplier->commission}} required autocomplete="commission" autofocus>
                            @error('commission')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="category_id">Candiadte city</label>
                                <select class="form-control" name="category_id">
                                    <option value="{{ $supplier->category->id }}">{{$supplier->category->name}}</option>
                                @foreach ($categories as $category)
                                    @if($supplier->category_id != $category->id)
                                        <option value="{{ $category->id }}">
                                            {{ $category->name }}
                                        </option>
                                    @endif
                                @endforeach
                                </select>
                        </div> 
                        <div>
                            <input TYPE="button" class="btn btn-primary" VALUE="Back" onClick="history.go(-1);">

                            <input type = "submit" class="btn btn-primary" name = "submit" value = "Update supplier">
                        </div>                       
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>      
@endsection


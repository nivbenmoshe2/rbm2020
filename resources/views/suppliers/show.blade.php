@extends('layouts.app')

@section('title', 'Supplier Page')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>Supplier Details</h1></div>        
                    <div class="card-body">
                        <table class = "table table-striped">
                            <tr>
                                <th></th><th>Details</th>
                            </tr>
                            <!-- the table data -->
                            <tr><td>Id</td><td>{{$supplier->id}}</td></tr>
                            <tr><td>Name</td><td>{{$supplier->name}}</td></tr>
                            <tr><td>Phone</td><td>{{$supplier->phone}}</td></tr>
                            <tr><td>Contact</td><td>{{$supplier->contact}}</td></tr>
                            <tr><td>Address</td><td>{{$supplier->address}}</td></tr>
                            <tr><td>Commission</td><td>{{$supplier->commission}}%</td></tr>
                            <tr><td>Status</td><td>{{$supplier->status->name}}</td></tr>  
                            <tr><td>Category</td><td>{{$supplier->category->name}}</td></tr>                      
                            <tr><td>Created</td><td>{{$supplier->created_at}}</td></tr>
                            <tr><td>Updated</td><td>{{$supplier->updated_at}}</td></tr>                                                              
                        </table>
                        <div>
                            <input TYPE="button" class="btn btn-primary" VALUE="Back" onClick="history.go(-1);">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection


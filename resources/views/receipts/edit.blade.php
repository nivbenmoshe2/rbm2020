@extends('layouts.app')

@section('title', 'Edit receipt')

@section('content') 
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit receipt</div>        
                    <div class="card-body">      
                        <form method = "post" action = "{{action('ReceiptsController@update',$receipt->id)}}">
                        @csrf
                        @METHOD('PATCH')
                        <div class="form-group">
                            <label for = "number">Reference No</label>
                            <input type = "number" class="form-control @error('number') is-invalid @enderror" name = "number" value = {{$receipt->number}} required autocomplete="number" autofocus>
                            @error('number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="customer_id">Customer</label>
                                <select class="form-control" name="customer_id">
                                    <option value="{{ $receipt->customer->id }}">{{$receipt->customer->name}}</option>
                                @foreach ($customers as $customer)
                                    @if($receipt->customer_id != $customer->id)
                                        <option value="{{ $customer->id }}">
                                            {{ $customer->name }}
                                        </option>
                                    @endif
                                @endforeach
                                </select>
                        </div>
                        <div class="form-group">
                            <label for = "cash">Cash</label>
                            <input type = "number" class="form-control @error('cash') is-invalid @enderror" name = "cash" value = {{$receipt->cash}} required autocomplete="cash" autofocus>
                            @error('cash')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>     
                        <div class="form-group">
                            <label for = "bit">Bit</label>
                            <input type = "number" class="form-control @error('bit') is-invalid @enderror" name = "bit" value = {{$receipt->bit}} required autocomplete="bit" autofocus>
                            @error('bit')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> 
                        <div class="form-group">
                            <label for = "date">Date</label>
                            <input type = "date" class="form-control @error('date') is-invalid @enderror" name = "date" value = {{$receipt->date}} required autocomplete="date" autofocus>
                            @error('date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div>
                            <input TYPE="button" class="btn btn-primary" VALUE="Back" onClick="history.go(-1);">
                            <input type = "submit" class="btn btn-primary" name = "submit" value = "Update supplier">
                        </div>                       
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>      
@endsection


@extends('layouts.app')

@section('title', 'Receipts')

@section('content')



<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-10">
                            <h2>Receipts</h2>
                        </div>  
                        <a href =  "{{url('/receipts/create')}}" class="btn btn-sm btn-info"> Add new receipt</a>
                    </div>
                </div>
                    <div class="card-body">
                        <table class = "table table-striped">
                            <tr>
                                <th>Reference No</th><th>Customer</th><th>Total</th><th>Date</th><th>Assign</th><th></th>
                            </tr>
                            <!-- the table data -->
                            @foreach($receipts as $receipt)
                                    <tr>
                                        <td>{{$receipt->number}}</td>
                                        <td>@if(isset($receipt->customer_id))
                                            {{$receipt->customer->name}} 
                                            @else
                                            Assign customer
                                            @endif</td>
                                        <td>{{$receipt->cash+$receipt->bit}}</td>
                                        <td>{{date('d-m-Y', strtotime($receipt->date))}}</td>
                                        <td>
                                            @if(isset($receipt->status))
                                            <h5><small class="badge badge-success" role="alert"><strong>Assigned</strong></small></h5>
                                            @else
                                            <a href = "{{route('receipts.payment',$receipt->id)}}" class="btn btn-primary btn-sm">Assign meetings</a>
                                            @endif
                                        </td> 
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-dark" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <a class="dropdown-item" href="{{route('receipts.show',$receipt->id)}}">Details</a>
                                                    <a class="dropdown-item" href="{{route('receipts.edit',$receipt->id)}}">Edit</a>
                                                    @if($receipt->status == null)
                                                    <a class="dropdown-item" onclick="return confirm('Are you sure you would like to delete this receipt?');" href="{{route('receipt.delete',$receipt->id)}}">Delete</a>  
                                                    @endif
                                                </div>
                                            </div>
                                        </td>                                                               
                                    </tr>
                            @endforeach
                        </table>
                        {{$receipts->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection


@extends('layouts.app')

@section('title', 'Receipt Page')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>Receipt Details</h1></div>        
                    <div class="card-body">
                        <table class = "table table-striped">
                            <tr>
                                <th></th><th>Details</th>
                            </tr>
                            <!-- the table data -->
                            <tr><td>Id</td><td>{{$receipt->id}}</td></tr>
                            <tr><td>Number</td><td>{{$receipt->number}}</td></tr>
                            <tr><td>Customer</td><td>{{$receipt->customer->name}}</td></tr> 
                            <tr><td>Cash</td><td>{{$receipt->cash}}</td></tr>
                            <tr><td>Bit</td><td>{{$receipt->bit}}</td></tr>
                            <tr><td>Total</td><td>{{$receipt->cash+$receipt->bit}}</td></tr>
                            <tr><td>Date</td><td>{{$receipt->date}}</td></tr>                     
                            <tr><td>Created</td><td>{{$receipt->created_at}}</td></tr>
                            <tr><td>Updated</td><td>{{$receipt->updated_at}}</td></tr>                                                              
                        </table>
                        <div>
                            <input TYPE="button" class="btn btn-primary" VALUE="Back" onClick="history.go(-1);">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection


@extends('layouts.app')

@section('title', 'Create receipt')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create receipt</div>        
                    <div class="card-body">
                        <form method = "post" action = "{{action('ReceiptsController@store')}}">
                        @csrf 
                        <div class="form-group">
                            <label for = "number">Reference No</label>
                            <input type = "number" class="form-control @error('number') is-invalid @enderror" name = "number" required autocomplete="number" autofocus>
                            @error('number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> 
                        <div class="form-group" >
                            <label for="customer_id">Customer</label>
                                <select class="form-control" name="customer_id">                                                                          
                                @foreach ($customers as $customer)
                                    <option value="{{ $customer->id }}"> 
                                        {{ $customer->name }} 
                                    </option>
                                @endforeach    
                                </select>
                        </div>
                        <div class="form-group">
                            <label for = "cash">Cash</label>
                            <input type = "number" class="form-control @error('cash') is-invalid @enderror" name = "cash" required autocomplete="cash" autofocus>
                            @error('cash')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> 
                        <div class="form-group">
                            <label for = "bit">Bit</label>
                            <input type = "number" class="form-control @error('bit') is-invalid @enderror" name = "bit" required autocomplete="bit" autofocus>
                            @error('bit')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>     
                        <div class="form-group">
                            <label for = "date">Date</label>
                            <input type = "date" class="form-control @error('date') is-invalid @enderror" name = "date" required autocomplete="date" autofocus>
                            @error('date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div>     
                            <input TYPE="button" class="btn btn-primary" VALUE="Back" onClick="history.go(-1);">
       
                            <input type = "submit" class="btn btn-primary" name = "submit" value = "Create receipt">
                        </div>                       
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection

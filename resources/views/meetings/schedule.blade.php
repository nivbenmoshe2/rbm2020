@extends('layouts.app')

@section('title', 'Schedule')

@section('content')



<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-7">
                            <h2>Weekly Schedule</h2>
                            From: {{date('d-m-Y', strtotime($weekStartDate))}} Until: {{date('d-m-Y', strtotime($weekEndDate))}}
                        </div>  
                       @can('admin') 
                            <a href =  "{{url('/meetings/create')}}" class="btn btn-sm btn-info"> Add new business meeting</a><a href =  "{{url('/add')}}" class="btn btn-sm btn-info"> Add new personal meeting</a>
                        @endcan
                    </div>
                </div>      
                    <div class="card-body">
                        <form method="post" action="{{url('multipledelete')}}">
                            {{ csrf_field() }}
                            <div class="box-body">
                                @if(Session::has("Delete"))
                                    <div class="alert alert-success">
                                        <b>Successfull, The meeting has been deleted.</b>
                                    </div>
                                @endif       
                        @if(count($meetings) > 0)
                        <table class = "table table-striped">
                            <tr>
                                @can('admin')<th><input type="submit" onclick="return confirm('Are you sure you would like to delete there meetings?');" class="btn btn-danger" name="submit" value="Delete"/></th><th>Type</th>@endcan<th>Place</th><th>Day</th><th>Date</th><th>Start</th><th>End</th><th>Close</th><th></th>
                            </tr>
                            <!-- the table data -->
                            
                            @foreach($meetings as $meeting)
                                <tr>
                                    @can('admin')<td><input name='id[]' type="checkbox" id="checkItem" value="{{$meeting->id}}">
                                    <td>{{$meeting->type}}</td>@endcan
                                    <td>{{$meeting->place}}</td>
                                    <td>{{date('l', strtotime($meeting->date))}}</td>
                                    <td>{{date('d-m-Y', strtotime($meeting->date))}}</td>
                                    <td>{{date('H:i', strtotime($meeting->start))}}</td>
                                    <td>{{date('H:i', strtotime($meeting->end))}}</td>
                                    <td>
                                        @if(!isset($meeting->summary))
                                            @can('admin')
                                                @if($meeting->type == 'Business')
                                                    <a href = "{{route('meetings.close',$meeting->id)}}" class="btn btn-primary btn-sm">Close</a>
                                                @else
                                                    <a href = "{{route('closepersonal',$meeting->id)}}" class="btn btn-primary btn-sm">Close</a>
                                                @endif
                                            @endcan
                                        @else
                                            <h5><small class="badge badge-success" role="alert"><strong>Done</strong></small></h5>
                                        @endif
                                    </td> 
                                    <td class="text-right">
                                        <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-dark" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item" href="{{route('meetings.show',$meeting->id)}}">Details</a>
                                            @can('admin')   
                                                <a class="dropdown-item" href="{{route('meetings.edit',$meeting->id)}}">Edit</a>
                                                <a class="dropdown-item" onclick="return confirm('Are you sure you would like to delete this meeting?');" href="{{route('meeting.delete',$meeting->id)}}">Delete</a>  
                                            @endcan
                                        </div>
                                        </div>
                                    </td>                                                         
                                </tr>
                            @endforeach
                            
                        </table>
                        @else
                            <div class="alert alert-warning">
                                <b>No weekly meetings</b>
                            </div> 
                        @endif
                        </div>
                        </form>
                        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"> </script>
                        <script language="javascript">
                        $("#checkAll").click(function () {
                        $('input:checkbox').not(this).prop('checked', this.checked);
                        });
                        </script>
                        {{$meetings->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection


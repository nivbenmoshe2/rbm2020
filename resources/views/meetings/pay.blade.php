@extends('layouts.app')

@section('title', 'Assign Meetings')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header"><h2>Assign Meetings</h2></div>        
                    <div class="card-body">
                        <form method="post" action="{{url('multiplerecords')}}">
                            @if(Session::has("Successfull"))
                            <div class="alert alert-success">
                                <b>Successfull, Your updates have been saved.</b><br><a class="btn btn-primary" role="button" href="{{ url('/receipts') }}">Back to Receipts</a>
                            </div>
                        @endif
                            {{ csrf_field() }}
                            <div class="box-body">
                        <table class = "table table-striped">
                            <tr>
                                <th><input type="submit" class="btn btn-primary" name="submit" value="Paid"/></th><th>Customer</th><th>Place</th><th>Date</th><th>Start</th><th>End</th><th></th>
                            </tr>
                            <!-- the table data -->
                            @foreach($meetings as $meeting)
                                <tr>
                                    <td><input name='id[]' type="checkbox" id="checkItem" value="{{$meeting->id}}">
                                    <td>{{$meeting->customer->name}}</td>
                                    <td>{{$meeting->place}}</td>
                                    <td>{{date('d-m-Y', strtotime($meeting->date))}}</td>
                                    <td>{{date('H:i', strtotime($meeting->start))}}</td>
                                    <td>{{date('H:i', strtotime($meeting->end))}}</td>
                                    <td>
                                        <a class="btn btn-primary btn-sm" href = "{{route('meetings.show',$meeting->id)}}">Details</a>
                                    </td> 
                                                                                              
                                </tr>
                            @endforeach
                        </table>
                        </div>
                            
                        </form>
                        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"> </script>
                        <script language="javascript">
                        $("#checkAll").click(function () {
                        $('input:checkbox').not(this).prop('checked', this.checked);
                        });
                        </script>
                        {{$meetings->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection


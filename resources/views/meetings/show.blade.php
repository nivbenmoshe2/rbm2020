@extends('layouts.app')

@section('title', 'Meeting Page')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>Meeting Details</h1></div>        
                    <div class="card-body">
                        <table class = "table table-striped">
                            <tr>
                                <th></th><th>Details</th>
                            </tr>
                            <!-- the table data -->
                            <tr><td>Id</td><td>{{$meeting->id}}</td></tr>
                            <tr><td>Type</td><td>{{$meeting->type}}</td></tr>
                            <tr><td>Place</td><td>{{$meeting->place}}</td></tr>
                            <tr><td>Date</td><td>{{date('d-m-Y', strtotime($meeting->date))}}</td></tr>
                            <tr><td>Start</td><td>{{date('H:i', strtotime($meeting->start))}}</td></tr> 
                            <tr><td>End</td><td>{{date('H:i', strtotime($meeting->end))}}</td></tr>
                            @if($meeting->type == 'Personal')
                            <tr><td>Description</td><td>{{$meeting->description}}</td></tr>
                            @endif
                            @if($meeting->type == 'Business')
                            <tr><td>Equipment</td><td>{{$meeting->equipment}}</td></tr>
                            <tr><td>Receipt</td><td>@if(isset($meeting->receipt_id))
                                {{$meeting->receipt->number}}  
                              @else
                                
                              @endif</td></tr> 
                            <tr><td>Service</td><td>{{$meeting->service->name}}</td></tr> 
                            <tr><td>Customer</td><td>{{$meeting->customer->name}}</td></tr> 
                            <tr><td>Supplier</td><td>@if(isset($meeting->supplier_id))
                                {{$meeting->supplier->name}} 
                              @else
                                
                              @endif</td></tr>  
                              <tr><td>Summary</td><td>{{$meeting->summary}}</td></tr>                     
                            @endif
                            <tr><td>Created</td><td>{{$meeting->created_at}}</td></tr>
                            <tr><td>Updated</td><td>{{$meeting->updated_at}}</td></tr>                                                              
                        </table>
                        <div><input TYPE="button" class="btn btn-primary" VALUE="Back" onClick="history.go(-1);"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection


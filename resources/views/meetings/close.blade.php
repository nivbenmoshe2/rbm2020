@extends('layouts.app')

@section('title', 'Close meeting')

@section('content') 
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Close meeting</div>        
                    <div class="card-body">      
                        <form method = "post" action = "{{action('MeetingsController@update',$meeting->id)}}">
                            @if(Session::has("Successfull"))
                                <div class="alert alert-success">
                                    <b>Successfull, Your updates have been saved.</b><br><a class="btn btn-primary" role="button" href="{{ url('/schedule') }}">Back to Schedule</a><a class="btn btn-primary" role="button" href="{{ url('/meetings') }}"> Back to Meetings</a>
                                </div>
                            @endif
                        @csrf
                        @METHOD('PATCH')
                        <div class="form-group">
                            <label for = "summary">Summary</label>
                            <textarea class="form-control" name="summary" rows="3" required autocomplete="summary" autofocus></textarea>
        
                            @error('summary')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div>
                            <input TYPE="button" class="btn btn-primary" VALUE="Back" onClick="history.go(-1);">

                            <input type = "submit" class="btn btn-primary" name = "submit" value = "Close meeting">
                        </div>                       
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>      
@endsection


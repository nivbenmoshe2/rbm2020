@extends('layouts.app')

@section('title', 'Edit meeting')

@section('content') 
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit meeting</div>        
                    <div class="card-body">      
                        <form method = "post" action = "{{action('MeetingsController@update',$meeting->id)}}">
                            @if(Session::has("Successfull"))
                                <div class="alert alert-success">
                                    <b>Successfull, Your updates have been saved.</b><br><a class="btn btn-primary" role="button" href="{{ url('/schedule') }}">Back to Schedule</a><a class="btn btn-primary" role="button" href="{{ url('/meetings') }}"> Back to Meetings</a>
                                </div>
                            @endif
                        @csrf
                        @METHOD('PATCH')
                        <div class="form-group">
                            <label for = "place">Meeting place</label>
                            <input type = "text" class="form-control" name = "place" value = {{$meeting->place}} required autocomplete="place" autofocus>
                            @error('place')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for = "date">Meeting date</label>
                            <input type = "date" class="form-control" name = "date" value = {{$meeting->date}} required autocomplete="date" autofocus>
                            @error('date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>     
                        <div class="form-group">
                            <label for = "start">Meeting start</label>
                            <input type = "time" class="form-control" name = "start" value = {{$meeting->start}} required autocomplete="start" autofocus>
                            @error('start')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> 
                        <div class="form-group">
                            <label for = "end">Meeting end</label>
                            <input type = "time" class="form-control" name = "end" value = {{$meeting->end}} required autocomplete="end" autofocus>
                            @error('end')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> 
                        @if($meeting->type == 'Personal')
                        <div class="form-group">
                            <label for = "description">Meeting description</label>
                            <input type = "text" class="form-control" name = "description" value = {{$meeting->description}}>
                        </div>
                        @endif
                        @if($meeting->type == 'Business')
                        <div class="form-group">
                            <label for = "equipment">Meeting equipment</label>
                            <input type = "text" class="form-control" name = "equipment" value = {{$meeting->equipment}}>
                        </div>
                        <div class="form-group">
                            <label for="service_id">Service</label>
                                <select class="form-control" name="service_id">
                                    <option value="{{ $meeting->service->id }}">{{$meeting->service->name}}</option>
                                @foreach ($services as $service)
                                    @if($meeting->service_id != $service->id)
                                        <option value="{{ $service->id }}">
                                            {{ $service->name }}
                                        </option>
                                    @endif
                                @endforeach
                                </select>
                        </div>
                        <div class="form-group">
                            <label for="customer_id">Customer</label>
                                <select class="form-control" name="customer_id">
                                    <option value="{{ $meeting->customer->id }}">{{$meeting->customer->name}}</option>
                                @foreach ($customers as $customer)
                                    @if($meeting->customer_id != $customer->id)
                                        <option value="{{ $customer->id }}">
                                            {{ $customer->name }}
                                        </option>
                                    @endif
                                @endforeach
                                </select>
                        </div>
                        <div class="form-group">
                            <label for="supplier_id">Supplier</label>
                            @if(isset($meeting->supplier_id))
                                <select class="form-control" name="supplier_id">
                                    
                                            <option value="{{ $meeting->supplier->id }}">{{$meeting->supplier->name}}</option>
                                        @foreach ($suppliers as $supplier)
                                            @if($meeting->supplier_id != $supplier->id)
                                                <option value="{{ $supplier->id }}">
                                                    {{ $supplier->name }}
                                                </option>
                                            @endif
                                        @endforeach
                                @else
                                <select class="form-control" name="supplier_id">  
                                    <option selected></option>                                                                        
                                @foreach ($suppliers as $supplier)
                                    <option value="{{ $supplier->id }}"> 
                                        {{ $supplier->name }} 
                                    </option>
                                @endforeach    
                                </select>
                                @endif
                                </select>
                        </div>
                        @endif
                        <div>
                            <input TYPE="button" class="btn btn-primary" VALUE="Back" onClick="history.go(-1);">

                            <input type = "submit" class="btn btn-primary" name = "submit" value = "Update meeting">
                        </div>                       
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>      
@endsection


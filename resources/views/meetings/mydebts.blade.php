@extends('layouts.app')

@section('title', 'Debts')

@section('content')



<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header"><h2>My Debts</h2></div>        
                    <div class="card-body">
                        <table class = "table table-striped">
                            <tr>
                                <th>Place</th><th>Date</th><th>Start</th><th>End</th><th>Payment</th><th></th>
                            </tr>
                            <!-- the table data -->
                            @foreach($meetings as $meeting)
                                @if($meeting->receipt_id == null && $meeting->summary != null)
                                    <tr>
                                        <td>{{$meeting->place}}</td>
                                        <td>{{date('d-m-Y', strtotime($meeting->date))}}</td>
                                        <td>{{date('H:i', strtotime($meeting->start))}}</td>
                                        <td>{{date('H:i', strtotime($meeting->end))}}</td>
                                        <td>
                                            <h5><small class="badge badge-danger" role="alert"><strong>Need to pay</strong></small></h5>
                                        </td> 
                                        <td>
                                            <a class="btn btn-primary btn-sm" href = "{{route('meetings.show',$meeting->id)}}">Details</a>
                                        </td>                                                                
                                    </tr>
                                @endif
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection


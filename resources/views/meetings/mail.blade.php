@extends('layouts.app')

@section('title', 'Meetings Mail')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
            <h2><div class="card-header bg-primary text-white" style="text-align:center;"> Invitation Mail</div><br></h2>
                    <div class="card-body"> 
                    <form method="POST" action="{{url('send/email')}}">
                    @if(Session::has("Successfull"))
                            <div class="alert alert-success">
                            <b>Successfull, your email sent.</b><br><a class="btn btn-primary" role="button" href="{{ url('/business') }}"> Back to Meetings</a>
                            </div>
                        @endif
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label text-md-left">To Email: </label>
                            <div class="col-md-7">
                            <input id="email" type="email" class="form-control{{$errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{$meeting->customer->email}}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="message" class="col-sm-3 col-form-label text-md-left">Message: </label>
                            
                                <textarea class="form-control" name="message" id="message" cols="100" rows="10">
                                    Hi {{$meeting->customer->name}}, 
                                    our meeting on {{date('l', strtotime($meeting->date))}} {{date('d-m-Y', strtotime($meeting->date))}} at {{date('H:i', strtotime($meeting->start))}} in {{$meeting->place}}.

                                    Thank you,
                                    Ronit Ben-Moshe
                                </textarea>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-5">
                                <button type="submit" class="btn btn-primary"> Send </button>
                            </div>

                        </div>
                    </div>
                </div>
            </div> 
        </div> 
    </div>
</div>
@endsection



@extends('layouts.app')

@section('title', 'Personal Page')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>Personal Details</h1></div>        
                    <div class="card-body">
                        <table class = "table table-striped">
                            <tr>
                                <th></th><th>Details</th>
                            </tr>
                            <!-- the table data -->
                            <tr><td>Id</td><td>{{$customer->id}}</td></tr>
                            <tr><td>Name</td><td>{{$customer->name}}</td></tr>
                            <tr><td>Gender</td><td>{{$customer->gender}}</td></tr>
                            <tr><td>Phone</td><td>{{$customer->phone}}</td></tr>
                            <tr><td>Email</td><td>{{$customer->email}}</td></tr> 
                            <tr><td>Birthdate</td><td>{{$customer->birthdate}}</td></tr>
                            <tr><td>Address</td><td>{{$customer->address}}</td></tr>
                            <tr><td>Status</td><td>{{$customer->status->name}}</td></tr>                       
                            <tr><td>Created</td><td>{{$customer->created_at}}</td></tr>
                            <tr><td>Updated</td><td>{{$customer->updated_at}}</td></tr>                                                              
                        </table>
                        <div>
                            <input TYPE="button" class="btn btn-primary" VALUE="Back" onClick="history.go(-1);">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection


@extends('layouts.app')

@section('title', 'Edit customer')

@section('content') 
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit customer</div>        
                    <div class="card-body">      
                        <form method = "post" action = "{{action('CustomersController@update',$customer->id)}}">
                        @csrf
                        @METHOD('PATCH')
                        <div class="form-group">
                            <label for = "name">Customer name</label>
                            <input type = "text" class="form-control @error('name') is-invalid @enderror" name = "name" value = {{$customer->name}} required autocomplete="name" autofocus>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for = "phone">Customer phone</label>
                            <input type = "number" class="form-control @error('phone') is-invalid @enderror"class="form-control" name = "phone" value = {{$customer->phone}} required autocomplete="phone" autofocus>
                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>    
                        <div class="form-group">
                            <label for = "email">Customer email</label>
                            <input type = "email" class="form-control @error('email') is-invalid @enderror" name = "email" value = {{$customer->email}} required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>  
                        <div class="form-group">
                            <label for = "birthdate">Customer birthdate</label>
                            <input type = "date" class="form-control @error('birthdate') is-invalid @enderror" name = "birthdate" value = {{$customer->birthdate}}>
                        </div> 
                        <div class="form-group">
                            <label for = "address">Customer address</label>
                            <input type = "text" class="form-control @error('address') is-invalid @enderror" name = "address" value = {{$customer->address}} required autocomplete="address" autofocus>
                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> 
                        <div class="form-group">
                            <label for="gender">Customer gender</label>
                                <select class="form-control" name="gender">
                                    <option>{{$customer->gender}}</option>
                                    @if($customer->gender != 'Male')
                                        <option>Male</option>
                                    @else
                                    <option>Female</option>
                                    @endif
                                </select>
                        </div>
                        <div>
                            <input TYPE="button" class="btn btn-primary" VALUE="Back" onClick="history.go(-1);">
                            <input type = "submit" class="btn btn-primary" name = "submit" value = "Update customer">
                        </div>                       
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>      
@endsection


@extends('layouts.app')

@section('title', 'Create customer')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create customer</div>        
                    <div class="card-body">
                        <form method = "post" action = "{{action('CustomersController@store')}}">
                        @csrf 
                        <div class="form-group">
                            <label for = "name">Name</label>
                            <input type = "text" class="form-control @error('name') is-invalid @enderror" name = "name" required autocomplete="name" autofocus>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> 
                        <div class="form-group">
                            <label for = "phone">Phone</label>
                            <input type = "number" class="form-control @error('phone') is-invalid @enderror" name = "phone" required autocomplete="phone" autofocus>
                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> 
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> 
                        <div class="form-group">
                            <label for = "birthdate">Birthdate</label>
                            <input type = "date" class="form-control" name = "birthdate" >
                        </div>
                        <div class="form-group">
                            <label for = "address">Address</label>
                            <input type = "text" class="form-control @error('address') is-invalid @enderror" name = "address" required autocomplete="address" autofocus>
                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="gender">Gender</label>
                                <select class="form-control" name="gender">                                                                         
                                    <option selected>Male</option>
                                    <option>Female</option>
                                </select>
                        </div>
                        
                        <div>    
                            <input TYPE="button" class="btn btn-primary" VALUE="Back" onClick="history.go(-1);">
        
                            <input type = "submit" class="btn btn-primary" name = "submit" value = "Create customer">
                        </div>                       
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection

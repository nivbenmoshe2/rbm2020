@extends('layouts.app')

@section('title', 'Customers')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-10">
                            <h2>Customers</h2>  
                        </div> 

                        <a href = "{{url('/customers/create')}}" class="btn btn-sm btn-info"> Add new customer</a>
                    </div>    
                </div>  
                    <div class="card-body">
                        @if(Session::has("Successfull"))
                                <div class="alert alert-success">
                                    <b>Successfull, Your updates have been saved.</b>
                                </div>
                            @endif
                        <table class = "table table-striped">
                            <tr>
                                <th>Name</th><th>Phone</th><th>Email</th><th>Address</th><th>Status</th><th></th>
                            </tr>
                            <!-- the table data -->
                            @foreach($customers as $customer)
                                <tr>
                                    <td>{{$customer->name}}</td>
                                    <td>{{$customer->phone}}</td>
                                    <td>{{$customer->email}}</td>
                                    <td>{{$customer->address}}</td>
                                    <td><div class="dropdown">
                                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{$customer->status->name}}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            @foreach($statuses as $status)
                                            <a class="dropdown-item" href="{{route('customers.changestatus', [$customer->id,$status->id])}}">{{$status->name}}</a>
                                            @endforeach
                                            </div></td>   
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-dark" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item" href="{{route('customers.show',$customer->id)}}">Details</a>
                                                <a class="dropdown-item" href="{{route('customers.edit',$customer->id)}}">Edit</a>
                                            </div>
                                        </div>
                                    </td>                                                                                       
                                </tr>
                            @endforeach
                        </table>
                        {{$customers->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection

